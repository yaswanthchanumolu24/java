package web;

import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;

// Youtube link : https://youtu.be/WKOoAQTioNU?si=QU4SjMCXYcr7J_DA (for better understanding)
//

//jar files 
// 1) https://javaee.github.io/javamail/
// 2) https://mvnrepository.com/artifact/javax.activation/activation/1.1.1

// Email : login into this gmail *(from this gmail we will sent mails to our identites in application)*
//  aprilfreshersbatch@gmail.com
// password: April15@


public class Mail {
    static Session newSession;
    private static MimeMessage message;

    public static void sendMail(String toMail,String subject,String msg) {
        Mail.setupServerProperties();
        Mail.draftEmail(toMail,subject,msg);
        Mail.send();
    }

    private static void send() {
        try {
            Transport.send(message);
            System.out.println("Sent message successfully....");
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    private static void draftEmail(String toMail,String subject,String msg) {
        try {
            message = new MimeMessage(newSession);
            message.setFrom(new InternetAddress("aprilfreshersbatch@gmail.com"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(toMail));
            message.setSubject(subject);
            message.setText(msg);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    private static void setupServerProperties() {
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.gmail.com.");
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        newSession = Session.getInstance(properties,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication("aprilfreshersbatch@gmail.com", "vlejiggxemvpiagi");
                }
        });
    }
}

package web;
import java.sql.*;
import java.util.Random;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


@ManagedBean(name="forgotPassword")
@SessionScoped
public class ForgotPassword {
	private String email;
	private Connection con;
	private String emailerror="";
	private String userName="";
	private String generatedotp;
	private String userotp;
	private String otperror;
	private String newPassword;
	private String passwordMsg;

	public ForgotPassword() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/bridgesoft","root","root");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public Connection getCon() {
		return con;
	}

	public void setCon(Connection con) {
		this.con = con;
	}

	public String getEmailerror() {
		return emailerror;
	}

	public void setEmailerror(String emailerror) {
		this.emailerror = emailerror;
	}

	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getGeneratedotp() {
		return generatedotp;
	}
	public void setGeneratedotp(String generatedotp) {
		this.generatedotp = generatedotp;
	}

	public String getUserotp() {
		return userotp;
	}
	public void setUserotp(String userotp) {
		this.userotp = userotp;
	}
	public String getOtperror() {
		return otperror;
	}
	public void setOtperror(String otperror) {
		this.otperror = otperror;
	}
	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	public String getPasswordMsg() {
		return passwordMsg;
	}

	public void setPasswordMsg(String passwordMsg) {
		this.passwordMsg = passwordMsg;
	}

	public String updatePassword() {
		if(con!=null) {
            try {
                PreparedStatement st=con.prepareStatement("update identity set password=? where username=? and email=?");
                System.out.println("-----------------------------------------");
                System.out.println("Email : " + this.email);
                System.out.println("UserName : " + this.userName);
                System.out.println("password : "+ this.newPassword);
                st.setString(1, this.getNewPassword());
                st.setString(2, this.getUserName());
                st.setString(3, this.getEmail());
                
                boolean isUpperCase=false;
                boolean isLowerCase=false;
                boolean isDigit=false;
                for(char eachChar: this.getNewPassword().toCharArray()) {
                	if(Character.isUpperCase(eachChar)) {
                		isUpperCase=true;
                	}else if(Character.isLowerCase(eachChar)) {
                		isLowerCase=true;
                	}else if(Character.isDigit(eachChar)) {
                		isDigit=true;
                	}
                }
                if(this.getNewPassword().length()>=8 && isUpperCase && isLowerCase && isLowerCase && isDigit) {
                	int rowsEffected=st.executeUpdate();
                	
                	if(rowsEffected>0) {
                		this.setPasswordMsg("Password got Updated");
                		return "index";
                	}
                }else {
            		this.setPasswordMsg("It must be at least 8 characters long.  \r\n"
            				+ "It must contain at least one lowercase letter. \r\n"
            				+ "It must contain at least one uppercase letter. \r\n"
            				+ "It must contain at least one number.");
            	}
                
                
                
            }catch(SQLException e) {
                System.out.println("Something Went wrong when try to fetch identity table");
                
            }

        }else {
            System.out.println("Connection Not established !!");
        }
		return null;
		
	}

	public void createOTP() {
		Random random = new Random();
		String result="";
		for(int i=0;i<6;i++) {
			result+=String.valueOf(random.nextInt(10));
		}
		this.setGeneratedotp(result);

        
	}
	public String sendEmail() {
		
		createOTP();
		String sub="CarService App - Password Reset Verification Code";
		String msg="Dear "+this.userName+",\r\n"
				+ "\r\n"
				+ "We received a request to reset your password for your CarService App account. \r\n"
				+ "\r\n"
				+ "Your verification code is: "+this.generatedotp +"\r\n"
				+ "Please enter this code to proceed with resetting your password. \r\n"
				+ " If you did not request a password reset, please ignore this email or contact support if you have any questions.\r\n"
				+ "\r\n"
				+ "Thank you for using CarService App!\r\n"
				+ "\r\n"
				+ "Best Regards,\r\n"
				+ "CarService Support Team";
		Mail.sendMail(this.email, sub, msg);
		return "";
		

	}
	public boolean isValidOTP() {
		System.out.println("Generated OTP : "+this.generatedotp);
		System.out.println("USer OTP : "+ this.userotp);
		return this.generatedotp.equals(this.userotp);
	}
	
	
	public boolean isValidEmail(String email) {
	
		if(con!=null) {
            try {
                PreparedStatement st=con.prepareStatement("select * from identity where email=?");
                st.setString(1, email);
                ResultSet rs= st.executeQuery();
                if(rs.next()) {
                	this.setUserName(rs.getString("username"));
                	return true;
                }else {
                	return false;
                }
                
                
            }catch(SQLException e) {
                System.out.println("Something Went wrong when try to fetch identity table");
                
            }

        }else {
            System.out.println("Connection Not established !!");
        }
        return false;
		
	}
	public String submit() {
		
		if(this.email.isEmpty()) {
			this.emailerror="Email Shouldn't be empty";
		}else {
			if(this.email.endsWith("@gmail.com")) {
				if(this.isValidEmail(email)) {
					sendEmail();
//					System.out.println(this.generatedotp);
					return "emailverification";
					
				}else {
					this.emailerror="No User Found with that Email !";
					}
			}else {
				this.emailerror="Please Enter the valid Email !!"	;		
			}
		}

		return "";
		
	}
	public String verify() {
		
		System.out.println();
		if(this.isValidOTP()) {
			System.out.println("otp was correct");
			return "resetPassword";
		}else {
			this.otperror="Invalid OTP !!";
		}
		return "";
	}
	public String reSendEmail() {
		this.otperror="We have successfully resent the email. Please check your inbox.";
		this.sendEmail();
		return "";
	}
	

}

package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.DbConnection;
import com.model.Customer;

public class CustomerDao {
	
	public Customer customerLogin(String email , String password){
		 
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String loginQuery = "Select * from identity where email = ? and password =?";
		
		try{
			pst = con.prepareStatement(loginQuery);
			pst.setString(1, email);
			pst.setString(2,  password);
			rs = pst.executeQuery();
			
			if(rs.next()){
				Customer customer = new Customer();
				
				customer.setUserName(rs.getString(1));
				customer.setDisplayName(rs.getString(2));
				customer.setFirstName(rs.getString(3));
				customer.setLastName(rs.getString(4));
				customer.setEmail(rs.getString(5));
				customer.setPassword(rs.getString(6));
				
				
				return customer;
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		finally{
			if(con !=null){
				try{
					rs.close();
					pst.close();
					con.close();
				}catch (SQLException e){
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
	public int customerRegister(Customer customer){
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		String insertQuery = "Insert into identity " + 
		"(userName , displayName , firstName , lastName , email , password) values(?,?,?,?,?,?)";
		
		try{
			pst = con.prepareStatement(insertQuery);
			pst.setString(1, customer.getUserName());
			pst.setString(2, customer.getDisplayName());
			pst.setString(3, customer.getFirstName());
			pst.setString(4, customer.getLastName());
			pst.setString(5, customer.getEmail());
			pst.setString(6, customer.getPassword());
			
			return pst.executeUpdate();
			
		} catch (SQLException e){
			e.printStackTrace();
		}
		
		finally{
			if(con != null){
				try{
					pst.close();
					con.close();
				}catch (SQLException e){
					e.printStackTrace();
				}
			}
		}
		return 0;
		
	}

}

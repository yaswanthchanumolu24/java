package com.model;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class UserSessionBean {
    private String userName;
    // Add other properties and methods as needed

    // Getters and setters
    public String getUsername() {
        return userName;
    }

    public void setUsername(String username) {
        this.userName = username; 
    }

}
package com.student;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.StudentDao;
import com.model.Student;


@RestController
public class StudentController {
	
        //Implementing Dependency Injection for ProductDao 
	@Autowired
	StudentDao studentDao;
	
	@GetMapping("getAllStudents")
	public List<Student> getAllStudents() {		
		return studentDao.getAllStudents();
	}
	
	@GetMapping("getStudentById/{rollNo}")
	public Student getStudentById(@PathVariable("rollNo") int rollNo) {
		return studentDao.getStudentById(rollNo);	
	}
	
	@GetMapping("getStudentByName/{studentName}")
	public List<Student> getStudentByName(@PathVariable("studentName") String studentName) {
		return studentDao.getStudentByName(studentName);
	}
	
	@PostMapping("addStudent")
	public Student addStudent(@RequestBody Student student) {
		return studentDao.addStudent(student);
	}
	
	@DeleteMapping("deleteStudent/{rollNo}")
	public Student deleteStudent(@PathVariable("rollNo") int rollNo){
		return studentDao.deleteStudent(rollNo);
	}
	
	@PutMapping("updateStudent/{rollNo}")
	public Student updateStudent(@PathVariable("rollNo") int rollNo, @RequestBody Student updatedStudent) {
	    updatedStudent.setRollNo(rollNo);
	    return studentDao.updateStudent(updatedStudent);
	}
	
}
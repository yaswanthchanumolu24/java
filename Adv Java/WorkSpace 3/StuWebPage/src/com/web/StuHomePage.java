package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/StuHomePage")
public class StuHomePage extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter printwriter = response.getWriter();

		String emailId = request.getParameter("emailId");

		printwriter.println("<body bgcolor='lightyellow' text='green'>");
		printwriter.println("<h3 style='color:red;'>Welcome " + emailId + "!</h3>");

		printwriter.println("<form align='right'>");
		printwriter.println("<a href='LogoutServlet'>Logout</a>");
		printwriter.println("</form>");

		printwriter.println("<center><h1>Welcome to StudentHomePage</h1>");
		printwriter.println("</center>");
		printwriter.println("</body>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}

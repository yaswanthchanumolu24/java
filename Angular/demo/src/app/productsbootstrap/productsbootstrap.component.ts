import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-productsbootstrap',
  templateUrl: './productsbootstrap.component.html',
  styleUrl: './productsbootstrap.component.css'
})
export class ProductsbootstrapComponent {
  products: any;
  emailId: any;
  selectedProduct : any;
  cartProducts : any;


  constructor(private service : EmpService) {

    this.emailId = localStorage.getItem('emailId');
    this.cartProducts = [];
    this.products = [
      {
        id : 101,
        name: 'Apple i15 pro',
        description: '48MP Main: 24 mm, ƒ/1.78 aperture, second‑generation sensor‑shift optical image stabilisation, 100% Focus Pixels, support for super‑high‑resolution photos (24MP and 48MP)',
        price: 150000,
        imageUrl: './assets/mobile pics/apple.jpg'
      },
      {
        id : 102,
        name: 'IQOO Z3 pro 5G',
        description: 'Rear: Sports, Night, Portrait, Photo, Video, 64MP, Panorama, Slow Motion, Time Lapse, Pro,live photo, Document, Dual-View Video ',
        price: 28999,
        imageUrl: './assets/mobile pics/IQOO.jpg'
      },
      {
        id : 103,
        name: 'Redmi 12 5G ',
        description: ' Redmi 12 5G brings the biggest display* for passionate gamers and entertainment enthusiasts to enjoy multimedia at its best',
        price: 13499,
        imageUrl: './assets/mobile pics/MI pic.png'
      },
      {
        id : 104,
        name: 'One plus 7 pro',
        description: 'Performance. Effortlessly powerful. Experience true speed with up to 12 GB of RAM, the Qualcomm® Snapdragon™ 855 Mobile Platform, and up to 256 GB of storage.',
        price: 19999,
        imageUrl: './assets/mobile pics/one plus.jpg'
      },
      {
        id : 105,
        name: 'oppo A57',
        description: 'Oppo A57 ; Size, 6.56 inches, 103.4 cm2 (~84.0% screen-to-body ratio) ; Resolution, 720 x 1612 pixels, 20:9 ratio (~269 ppi density) ; OS, Android 12, ColorOS 12.1.',
        price: 40000,
        imageUrl: './assets/mobile pics/oppo-a15-1.jpg'
      },
      {
        id : 106,
        name: 'vivo V27',
        description: 'Discover the sleek vivo V27 with 50MP Sony IMX766V (OIS) camera, 6.78" AMOLED display, MediaTek Dimensity 7200 5G processor, 66W fast charging.',
        price: 26000,
        imageUrl: './assets/mobile pics/vivo pic.jpg'
      }
    ];
  }

  ngOnInit() {
  }
  addToCart(product:any){
    this.service.addToCart(product);
  }


}

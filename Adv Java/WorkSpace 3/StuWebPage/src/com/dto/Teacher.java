package com.dto;

public class Teacher {
	private int rollNo;
	private String StuName;
	private int marks;
	private String gender;
	private String emailId;
	private String password;

	public Teacher() {
		super();
	}

	public Teacher(int rollNo, String stuName, int marks, String gender, String emailId, String password) {
		super();
		this.rollNo = rollNo;
		this.StuName = stuName;
		this.marks = marks;
		this.gender = gender;
		this.emailId = emailId;
		this.password = password;
	}

	public int getRollNo() {
		return rollNo;
	}

	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}

	public String getStuName() {
		return StuName;
	}

	public void setStuName(String stuName) {
		StuName = stuName;
	}

	public int getMarks() {
		return marks;
	}

	public void setMarks(int marks) {
		this.marks = marks;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String toString() {
		return "Teacher [rollNo=" + rollNo + ", StuName=" + StuName + ", marks=" + marks + ", gender=" + gender
				+ ", emailId=" + emailId + ", password=" + password + "]";
	}
}

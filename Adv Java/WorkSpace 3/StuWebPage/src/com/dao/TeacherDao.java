package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.DbConnection;
import com.dto.Teacher;

public class TeacherDao {
	public Teacher techLogin(String emailId, String password) {

		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		String loginQuery = "Select * from schoolstu where emailId = ? and password = ?";

		try {
			pst = con.prepareStatement(loginQuery);
			pst.setString(1, emailId);
			pst.setString(2, password);
			rs = pst.executeQuery();

			if (rs.next()) {
				Teacher teacher = new Teacher();

				teacher.setRollNo(rs.getInt(1));
				teacher.setStuName(rs.getString(2));
				teacher.setMarks(rs.getInt(3));
				teacher.setGender(rs.getString(4));
				teacher.setEmailId(rs.getString(5));
				teacher.setPassword(rs.getString(6));

				return teacher;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	public int registerTeacher(Teacher tech) {

		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;

		String insertQuery = "Insert into schoolstu " + 
				"(stuName, marks, gender, emailId, password) values (?, ?, ?, ?, ?)";

		try {
			pst = con.prepareStatement(insertQuery);

			pst.setString(1, tech.getStuName());
			pst.setInt(2, tech.getMarks());
			pst.setString(3, tech.getGender());
			pst.setString(4, tech.getEmailId());
			pst.setString(5, tech.getPassword());

			return pst.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			if (con != null) {
				try {
					pst.close();
					con.close();

				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return 0;
	}
}

package com.yaswanth;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.CustomerDao;
import com.model.Customer;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		
		String email = req.getParameter("email");
		String password = req.getParameter("password");

			CustomerDao customerDao = new CustomerDao();
			Customer customer = customerDao.customerLogin(email , password);
			
			if(customer != null){
				out.println("hii " + email);
				RequestDispatcher rd=req.getRequestDispatcher("AfterLogin.html");  
				rd.forward(req, res); 
			}
			else{
				out.println("customer not found");
			}
		
		
	}

}

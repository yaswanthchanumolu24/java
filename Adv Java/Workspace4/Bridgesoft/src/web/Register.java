package web;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.dto.CustomerDao;
import com.model.Customer;

@ManagedBean
@SessionScoped
public class Register {
	
	private String username;
	private String firstname;
	private String lastname;
	private String email;
	private String password;
	private Long phonenumber;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Long getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(Long phonenumber) {
		this.phonenumber = phonenumber;
	}
	
	
	public boolean register() {
		CustomerDao customerDao = new CustomerDao();
		
		if(customerDao.customerRegister(username, firstname, lastname, email, password, phonenumber)) {
			return true;
		}
		return false;
		
		}
					
	public String submit() {
		if(this.register()) {
			return "index.xhtml";
		}
		else {
			return "register.xhtml";
		}
	}
	
	
}

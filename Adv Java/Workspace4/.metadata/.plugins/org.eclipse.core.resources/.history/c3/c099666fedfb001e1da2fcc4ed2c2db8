<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:h="http://xmlns.jcp.org/jsf/html"
	xmlns:f="http://xmlns.jcp.org/jsf/core">
<h:head>
    <title>Registration Page</title>
    <style>
        
        @import url("https://fonts.googleapis.com/css2?family=Open+Sans:wght@200;300;400;500;600;700&amp;display=swap");

		* {
		  margin: 0;
		  padding: 0;
		  box-sizing: border-box;
		  font-family: "Open Sans", sans-serif;
		}
		
		body {
		  display: flex;
		  align-items: center;
		  justify-content: center;
		  min-height: 100vh;
		  width: 100%;
		  padding: 0 10px;
		}
		
		body::before {
		  content: "";
		  position: absolute;
		  width: 100%;
		  height: 100%;
		  background: url("https://images.unsplash.com/photo-1503376780353-7e6692767b70?q=80&amp;w=2070&amp;auto=format&amp;fit=crop&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"), #000;
		  background-position: center;
		  background-size: cover;
		}
		
		.container {
		  width: 400px;
		  border-radius: 8px;
		  padding: 30px;
		  text-align: center;
		  border: 1px solid rgba(255, 255, 255, 0.5);
		  backdrop-filter: blur(7px);
		  -webkit-backdrop-filter: blur(7px);
		}
		
		form {
		  display: flex;
		  flex-direction: column;
		}
		
		h2 {
		  font-size: 2rem;
		  margin-bottom: 20px;
		  color: #fff;
		}
		
		.input-field {
		  position: relative;
		  border-bottom: 2px solid #ccc;
		  margin: 15px 0;
		}
		
		.input-field label {
		  position: absolute;
		  top: 50%;
		  left: 0;
		  transform: translateY(-50%);
		  color: #fff;
		  font-size: 16px;
		  pointer-events: none;
		  transition: 0.15s ease;
		}
		
		.input-field input {
		  width: 100%;
		  height: 40px;
		  background: transparent;
		  border: none;
		  outline: none;
		  font-size: 16px;
		  color: #fff;
		}
		
		.input-field input:focus~label,
		.input-field input:valid~label {
		  font-size: 0.8rem;
		  top: 10px;
		  transform: translateY(-120%);
		}
		
		.forget {
		  display: flex;
		  align-items: center;
		  justify-content: space-between;
		  margin: 25px 0 35px 0;
		  color: #fff;
		}
		
		#Save-login {
		  accent-color: #fff;
		}
		
		.forget label {
		  display: flex;
		  align-items: center;
		}
		
		.forget label p {
		  margin-left: 8px;
		}
		
		.container a {
		  color: #efefef;
		  text-decoration: none;
		}
		
		.container a:hover {
		  text-decoration: underline;
		}
		
		.submit-button {
            background: #fff;
            color: #000;
            font-weight: 600;
            border: none;
            padding: 12px 20px;
            cursor: pointer;
            border-radius: 3px;
            font-size: 16px;
            border: 2px solid transparent;
            transition: 0.3s ease;
            margin-top: 20px; 
            width: 100%;
        }

        .submit-button:hover {
            color: #fff;
            border-color: #fff;
            background: rgba(255, 255, 255, 0.15);
        }
		
		.Create-account {
		  text-align: center;
		  margin-top: 30px;
		  color: #fff;
		}
        
    </style>
</h:head>
<h:body>
    <div class="container">
        <h2>Registration Form</h2>
        <h:form>
            <h:messages globalOnly="true" style="color: red;" />
            <div class="input-field">
                <h:outputLabel for="username">UserName:</h:outputLabel>
                <h:inputText id="username" value="#{register.username}" oninput="hideLabel('usernameLabel') required="true" />
                <h:message for="username" style="color: red;" />
            </div>
            <div class="input-field">
                <h:outputLabel for="firstname">FirstName:</h:outputLabel>
                <h:inputText id="firstname" value="#{register.firstname}" required="true" />
                <h:message for="firstname" style="color: red;" />
            </div>
            <div class="input-field">
                <h:outputLabel for="lastname">LastName:</h:outputLabel>
                <h:inputText id="lastname" value="#{register.lastname}" required="true" />
                <h:message for="lastname" style="color: red;" />
            </div>
            <div class="input-field">
                <h:outputLabel for="email">E-Mail:</h:outputLabel>
                <h:inputText id="email" value="#{register.email}" required="true" />
                <h:message for="email" style="color: red;" />
            </div>
            <div class="input-field">
                <h:outputLabel for="password">PassWord:</h:outputLabel>
                <h:inputText id="password" value="#{register.password}" required="true" />
                <h:message for="password" style="color: red;" />
            </div>
            <div class="input-field">
                <h:outputLabel for="phonenumber">PhoneNumber:</h:outputLabel>
                <h:inputText id="phonenumber" value="#{register.phonenumber}" required="true" >
                	<f:validateLength minimum="10" maximum="10"></f:validateLength>
                </h:inputText>
                <h:message for="phonenumber" style="color: red;" />
            </div>
            <h:commandButton action="#{register.submit()}" value="Submit" styleClass="submit-button"/>
        </h:form>
    </div>
</h:body>
<script>
    function hideLabel(labelId) {
        var label = document.getElementById(labelId);
        if (label) {
            label.style.display = 'none';
        }
    }
</script>
</html>

import { Component } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrl: './products.component.css'
})
export class ProductsComponent {

  products = [
    {
      exercise: 'Chest',
      description: 'exercise: Bench Press , Machine fly',
      Price: "price: 500",
      imageUrl: './assets/chest.jpeg'
    },
    {
      exercise: 'Legs',
      description: 'exercise: Squats , Lunges',
      Price: "price: 500",
      imageUrl: './assets/legs.jpg'
    },
    {
      exercise: 'Biceps',
      description: 'exercise: Bicep curl , Barbell curl',
      Price: "price: 500",
      imageUrl: './assets/biceps.jpg'
    },
    {
      exercise: 'Triceps',
      description: 'exercise: Dips , Grip Bench Press',
      Price: "price: 500",
      imageUrl: './assets/triceps.jpg'
    },
    {
      exercise: 'ABS',
      description: 'exercise: Cable crunch , Planks',
      Price: "price: 500",
      imageUrl: './assets/abs.png'
    },
    {
      exercise: 'Full Body',
      description: 'exercise: Deadlift , Goblet squat',
      Price: "price: 1000", 
      imageUrl: './assets/full body.jpg'
    },
  ];

}

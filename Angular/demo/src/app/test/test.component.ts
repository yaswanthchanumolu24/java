import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit{

  name:string;
  age:number;
  details:any;
  hobbies:any;
  constructor(){
    this.name = "Yaswanth";
    this.age = 22;
    this.details = {
      city : "Hyderabad",
      pincode : 500062,
      state : "telangana",
      country : "India"
    }
    this.hobbies = [ "swiming" , "dancing" , "anime" ]
  }

  ngOnInit() {

  }

}

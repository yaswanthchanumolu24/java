package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.TeacherDao;
import com.dto.Teacher;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter printWriter = response.getWriter();

		String stuName = request.getParameter("stuName");
		int marks = Integer.parseInt(request.getParameter("marks"));
		String gender = request.getParameter("gender");
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");

		Teacher tech = new Teacher(0, stuName, marks, gender, emailId, password);

		TeacherDao techDao = new TeacherDao();
		int result = techDao.registerTeacher(tech);

		printWriter.println("<body bgcolor='lightyellow'>");
		printWriter.println("<center>");

		if (result > 0) {			
			printWriter.println("<h1 style='color:green;'>Student Registered Successfully!!!</h1><br/>");

			RequestDispatcher rd = request.getRequestDispatcher("Login.html");
			rd.include(request, response);

		} else {

			RequestDispatcher rd = request.getRequestDispatcher("Register.html");
			rd.include(request, response);

			printWriter.println("<h1 style='color:red;'>Student Registeration Failed!!!</h1><br/>");

		}
		printWriter.println("</center>");
		printWriter.println("</body>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}
